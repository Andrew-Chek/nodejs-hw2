const {loginRequest, registerRequest, 
    changePasswordRequest, getUserRequest, 
        deleteUserRequest, getUserNotesRequest,
            createNoteRequest, getNoteByIdRequest, updateNoteByIdRequest,
                checkNoteByIdRequest, deleteNoteByIdRequest} = require('./requests.js');
const funcDictionary = {}

function fillDictionary()
{
    funcDictionary['Login'] = loginRequest;
    funcDictionary['Register'] = registerRequest;
    funcDictionary['Change user password'] = changePasswordRequest;
    funcDictionary['Get user info'] = getUserRequest;
    funcDictionary['Delete user info'] = deleteUserRequest;
    funcDictionary['Get user notes'] = getUserNotesRequest;
    funcDictionary['Create user note'] = createNoteRequest;
    funcDictionary['Get note by id'] = getNoteByIdRequest;
    funcDictionary['Update note by id'] = updateNoteByIdRequest;
    funcDictionary['Check note by id'] = checkNoteByIdRequest;
    funcDictionary['Delete note by id'] = deleteNoteByIdRequest;
}

function addDataPopupEvent(btn)
{
    fillDictionary();
    btn.addEventListener('click', function(){
        if(btn.classList.contains('popup1__btn'))
        {
            const label = document.querySelector("#popup1 .up__label");
            const nameLabel = document.querySelector(".form__username .form__label");
            const pswLabel = document.querySelector(".form__password .form__label");
            const nameInput = document.querySelector(".form__username .checked_items");
            const pswInput = document.querySelector(".form__password .checked_items");
            nameInput.value = "";
            pswInput.value = "";
            const submit = document.querySelector(".form__submit .form__btn");

            label.innerText = btn.innerText;
            console.log(submit.innerText);
            submit.innerText = btn.innerText;
            submit.addEventListener('click', funcDictionary[btn.innerText])

            if(btn.getAttribute('id') == 'password')
            {
                nameLabel.innerText = 'Old password'
                pswLabel.innerText = 'New password'
                nameInput.setAttribute('placeholder', 'Enter old password')
                pswInput.setAttribute('placeholder', 'Enter new password')
            }
            else if(btn.getAttribute('id') == 'update')
            {
                nameLabel.innerText = 'Id'
                pswLabel.innerText = 'Text'
                nameInput.setAttribute('placeholder', 'Enter note id')
                pswInput.setAttribute('placeholder', 'Enter new note text')
            }
            else
            {
                nameLabel.innerText = 'Username'
                pswLabel.innerText = 'Password'
                nameInput.setAttribute('placeholder', 'Enter username')
                pswInput.setAttribute('placeholder', 'Enter password')
            }
        }
        else if(btn.classList.contains('popup2__btn'))
        {
            const label = document.querySelector("#popup2 .up__label");
            const nameLabel = document.querySelector(".form__id .form__label");
            const nameInput = document.querySelector(".form__id .checked_items");
            nameInput.value = "";
            const submit = document.querySelector("#popup2 .form__submit .form__btn");

            label.innerText = btn.innerText;
            submit.innerText = btn.innerText;
            submit.addEventListener('click', funcDictionary[btn.innerText])

            if(btn.getAttribute('id') == 'text')
            {
                nameLabel.innerText = 'Text'
                nameInput.setAttribute('placeholder', 'Enter note text')
            }
            else
            {
                nameLabel.innerText = 'Note id'
                nameInput.setAttribute('placeholder', 'Enter note id')
            }
        }
    });
}

function addRequestEvents()
{
    const btnsWithoutPopup = document.querySelectorAll('.without_btn');
    btnsWithoutPopup.forEach(btn => {
        btn.addEventListener('click', funcDictionary[btn.innerText])
    })
}

module.exports = {
    addDataPopupEvent, addRequestEvents
};