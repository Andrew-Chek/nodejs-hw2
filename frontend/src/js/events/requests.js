async function loginRequest()
{
    const userDiv = document.querySelector('.content__user');
    const notesDiv = document.querySelector('.content__notes');
    const usernameInput = document.querySelector(".profile__username");
    const nameInput = document.querySelector(".form__username .checked_items");
    const pswInput = document.querySelector(".form__password .checked_items");
    const username = nameInput.value;
    const password = pswInput.value;

    const res = await fetch('http://localhost:8080/api/auth/login', {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({username, password}),
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${text.message}`;

    if(res.status == 200)
    {
        usernameInput.value = username;
        const myStorage = window.localStorage;
        myStorage.setItem('jwt', text.jwt_token);
        userDiv.style.display = 'block'
        notesDiv.style.display = 'block'
    }

    const btn = document.querySelector(".form__submit .form__btn");
    btn.removeEventListener('click', loginRequest)
}

async function registerRequest()
{
    const nameInput = document.querySelector(".form__username .checked_items");
    const pswInput = document.querySelector(".form__password .checked_items");
    const username = nameInput.value;
    const password = pswInput.value;

    const res = await fetch('http://localhost:8080/api/auth/register', {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({username, password}),
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${text.message}`;

    const btn = document.querySelector(".form__submit .form__btn");
    btn.removeEventListener('click', registerRequest)
}

async function changePasswordRequest()
{
    const nameInput = document.querySelector(".form__username .checked_items");
    const pswInput = document.querySelector(".form__password .checked_items");
    const oldPassword = nameInput.value;
    const newPassword = pswInput.value;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/users/me', {
        method: 'PATCH', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({oldPassword, newPassword}),
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${text.message}`;

    const btn = document.querySelector(".form__submit .form__btn");
    btn.removeEventListener('click', changePasswordRequest)
}

async function getUserRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/users/me', {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${JSON.stringify(text)}`;
}

async function deleteUserRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/users/me', {
        method: 'DELETE', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${text.message}`;

    if(res.status == 200)
    {
        const userDiv = document.querySelector('.content__user');
        const notesDiv = document.querySelector('.content__notes');
        const usname = document.querySelector('.profile__username');

        userDiv.style.display = 'none';
        notesDiv.style.display = 'none';
        usname.value = '';
    }
}

async function getUserNotesRequest()
{
    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/notes', {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const text = await res.json();
    response.innerText = `message: ${JSON.stringify(text)}`;
}

async function createNoteRequest()
{
    const textInput = document.querySelector(".form__id .checked_items");
    const text = textInput.value;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch('http://localhost:8080/api/notes', {
        method: 'POST', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({text}),
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const result = await res.json();
    response.innerText = `message: ${result.message}`;

    const btn = document.querySelector(".form__submit2");
    btn.removeEventListener('click', createNoteRequest)
}

async function getNoteByIdRequest()
{
    const idInput = document.querySelector(".form__id .checked_items");
    const id = idInput.value;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/notes/${id}`, {
        method: 'GET', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const result = await res.json();
    response.innerText = `message: ${JSON.stringify(result)}`;

    const btn = document.querySelector(".form__submit2");
    btn.removeEventListener('click', getNoteByIdRequest)
}

async function updateNoteByIdRequest()
{
    const idInput = document.querySelector(".form__username .checked_items");
    const textInput = document.querySelector(".form__password .checked_items");
    const id = idInput.value;
    const text = textInput.value;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/notes/${id}`, {
        method: 'PUT', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({text}),
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const result = await res.json();
    response.innerText = `message: ${result.message}`;

    const btn = document.querySelector(".form__submit .form__btn");
    btn.removeEventListener('click', updateNoteByIdRequest)
}

async function checkNoteByIdRequest()
{
    const idInput = document.querySelector(".form__id .checked_items");
    const id = idInput.value;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/notes/${id}`, {
        method: 'PATCH', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const result = await res.json();
    response.innerText = `message: ${result.message}`;

    const btn = document.querySelector(".form__submit2");
    btn.removeEventListener('click', checkNoteByIdRequest)
}

async function deleteNoteByIdRequest()
{
    const idInput = document.querySelector(".form__id .checked_items");
    const id = idInput.value;

    const myStorage = window.localStorage;
    const jwt = myStorage.getItem('jwt')

    const res = await fetch(`http://localhost:8080/api/notes/${id}`, {
        method: 'DELETE', 
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        },
        mode: 'cors'
    })
    const response = document.querySelector(".response__text")
    const result = await res.json();
    response.innerText = `message: ${result.message}`;

    const btn = document.querySelector(".form__submit2");
    btn.removeEventListener('click', deleteNoteByIdRequest)
}

module.exports = {
    loginRequest, 
    registerRequest, 
    changePasswordRequest,
    getUserRequest,
    deleteUserRequest,
    getUserNotesRequest,
    createNoteRequest,
    getNoteByIdRequest,
    updateNoteByIdRequest,
    checkNoteByIdRequest,
    deleteNoteByIdRequest
}