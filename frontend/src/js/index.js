const {addDataPopupEvent, addRequestEvents} = require('./events/requestEvents.js');

const btns = document.querySelectorAll('.popup_btn');
btns.forEach(btn => {
    addDataPopupEvent(btn);
})

addRequestEvents();