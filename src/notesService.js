const { Note } = require('./models/Notes.js');

async function createNote(req, res, next) {
  const { text } = req.body;
  const createdDate = new Date().toISOString();
  const note = new Note({
    text,
    createdDate,
    userId: req.user.userId
  });
  await note.save()
  res.status(200).json({message: "Success"})
}

const deleteMyNoteById = (req, res, next) => Note.findByIdAndDelete({_id: req.params.id, userId: req.user.userId})
  .then(
    res.status(200).json({ message: "Success" }));

const getMyNotes = (req, res, next) => {
  return Note.find({userId: req.user.userId}, '-__v').skip(req.query.offset).limit(req.query.limit).then((result) => {
    res.status(200).json({
      "offset": req.query.offset,
      "limit": req.query.limit,
      "count": result.length,
      "notes": result
    });
  });
}

const updateMyNoteById = async (req, res, next) => {
  const { text } = req.body;
  if(await checkExistingNote(req.params.id))
  {
    return Note.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { text } })
      .then((result) => {
        res.json({message: 'Success'});
      });
  }
  else
  {
    return res.status(400).json({message: "There is no note with such id"})
  }
}

const checkMyNoteById = async (req, res, next) => {
  if(await checkExistingNote(req.params.id))
  {
    return Note.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { completed: true } })
    .then((result) => {
      res.json({message: 'Success'});
    });
  }
  else
  {
    return res.status(400).json({message: "There is no note with such id"})
  }
}

const checkExistingNote = async (id) =>
{
  try{
    const note = await Note.findById(id);
    return note != null;
  }
  catch
  {
    return false
  }
}

const getMyNoteById = async (req, res, next) => {
  if( await checkExistingNote(req.params.id))
  {
    return Note.findById({_id: req.params.id, userId: req.user.userId})
      .then((note) => {
        res.status(200).json({
          "note":
            {
              _id:note.id, 
              userId: note.userId, 
              completed: note.completed, 
              text:note.text,
              createdDate: note.createdDate
            }});
      });
  }
  else
  {
    return res.status(400).json({message: "There is no note with such id"})
  }
}

module.exports = {
  createNote,
  getMyNotes,
  getMyNoteById,
  checkMyNoteById,
  deleteMyNoteById,
  updateMyNoteById,
};
