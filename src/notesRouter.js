const express = require('express');
const router = express.Router();
const {
  createNote, getMyNoteById, deleteMyNoteById, checkMyNoteById, getMyNotes, updateMyNoteById
} = require('./notesService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getMyNotes);

router.get('/:id', authMiddleware, getMyNoteById);

router.put('/:id', authMiddleware, updateMyNoteById);

router.patch('/:id', authMiddleware, checkMyNoteById);

router.delete('/:id', authMiddleware, deleteMyNoteById);

module.exports = {
  notesRouter: router,
};
