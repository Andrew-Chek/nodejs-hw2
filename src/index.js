const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://Endry_Chek:RvbUaLi9qUGb9LT@cluster0.rqlw2dd.mongodb.net/?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { authentificateRouter } = require('./authentificateRouter.js');

app.use(cors());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', authentificateRouter);
app.use('/api/users', usersRouter);

const start = async () => {
    app.listen(8080);
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  if(err != null)
  {
    res.status(400).send({ message: err.message });
  }
  else
  {
    res.status(500).send({ message: 'Server error' });
  }
}
