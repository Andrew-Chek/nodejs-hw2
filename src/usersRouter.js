const express = require('express');
const router = express.Router();
const { getUser, deleteUser, changeUserPassword } = require('./usersService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/me', authMiddleware, getUser);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me', authMiddleware, changeUserPassword);

module.exports = {
  usersRouter: router,
};
