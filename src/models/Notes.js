const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  }
});

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note,
};
