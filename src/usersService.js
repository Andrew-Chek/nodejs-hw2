const { User } = require('./models/Users.js');
const { Credential } = require('./models/Credentials.js');
const { Error } = require('./models/Errors.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;
  if(username == "" || password == "")
  {
    return res.status(400).json({message: "Not enough data for registration"});
  }
  const checkUser = await User.findOne({username: req.body.username})
  if(checkUser != undefined)
  {
    return res.status(400).json({message: "User with such username is already existing"});
  }

  const credential = new Credential({
    username,
    password: await bcrypt.hash(password, 10)
  });

  const createdDate = new Date().toISOString()

  const user = new User({
    username,
    createdDate: createdDate
  })

  await credential.save()
    .catch(err => {
      next(err);
    });

  await user.save()
    .catch(err => {
      next(err);
    });
  return res.status(200).json({message: "Success"})
}

const loginUser = async (req, res, next) => {
  const credential = await Credential.findOne({ username: req.body.username });
  if(!credential)
  {
    const error = new Error({
      message: `No such user`
    });
    error.save()
    return res.status(400).json({message: error.message})
  }
  if (await bcrypt.compare(String(req.body.password), String(credential.password))) {
    const user = await User.findOne({ username: req.body.username });
    const payload = { username: user.username, createdDate: user.createdDate, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.json({message: "Success", "jwt_token": jwtToken});
  }
  else
  {
    const error = new Error({
      message: `Password isn't correct`
    });
    error.save()
    return res.status(400).json({message: error.message})
  }
}

const getUser = async (req, res, next) => {
  const user = await User.findById( req.user.userId );
  res.status(200).send({
    user: {
      _id: user.id,
      username: user.username,
      createdDate: user.createdDate
    }
  });
};

const changeUserPassword = async (req, res, next) =>
{
  const credential = await Credential.findOne({username: req.user.username})
	if (await bcrypt.compare(String(req.body.oldPassword), String(credential.password)))
  {
    const newPassword = await bcrypt.hash(req.body.newPassword, 10)
    return Credential.findOneAndUpdate({username: req.user.username}, {$set: { password: newPassword }})
    .then((result) => {
      res.status(200).json({message: 'Success'});
    });
  }
  else
  {
    const err = new Error({
      message: `Old password isn't correct`
    });
    err.save()
    return res.status(400).json({message: err.message})
  }
}

const deleteUser = async(req, res, next) => 
{
  await User.findByIdAndDelete(req.user.userId)
  await Credential.findOneAndDelete({username: req.user.username})
  return res.status(200).json({ message: "Success"})
}

module.exports = {
  registerUser,
  loginUser,
  getUser,
  deleteUser,
  changeUserPassword
};
